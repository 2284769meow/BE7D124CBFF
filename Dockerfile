FROM nvidia/cuda:10.1-cudnn7-devel-ubuntu18.04

# Install some basic utilities
RUN apt-get update && apt-get install -y \
    curl \
    ca-certificates \
    sudo \
    git \
    bzip2 \
    ffmpeg \
    libx11-6 \
    build-essential \
    pkg-config \
    cmake \
    make \
    libgoogle-perftools-dev \
 && rm -rf /var/lib/apt/lists/*


## Create a working directory
RUN mkdir /app
WORKDIR /app
COPY ./app /app

# Create a non-root user and switch to it
RUN adduser --disabled-password --gecos '' --shell /bin/bash user \
 && chown -R user:user /app
RUN echo "user ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/90-user

USER user

# All users can use /home/user as their home directory
ENV HOME=/home/user
RUN chmod 777 /home/user

# Install Miniconda and Python 3.8
ENV CONDA_AUTO_UPDATE_CONDA=false
ENV PATH=/home/user/miniconda/bin:$PATH
RUN curl -sLo ~/miniconda.sh https://repo.continuum.io/miniconda/Miniconda3-py38_4.8.2-Linux-x86_64.sh \
 && chmod +x ~/miniconda.sh \
 && ~/miniconda.sh -b -p ~/miniconda \
 && rm ~/miniconda.sh \
 && conda install -y python==3.8 \
 && conda clean -ya


# CUDA 10.1-specific steps
RUN conda install -y pytorch==1.7.0 -c pytorch
RUN conda install torchvision>=0.8.1 -c pytorch
RUN conda install cudatoolkit=10.1 -c pytorch
RUN conda clean -ya

RUN conda install pip

WORKDIR /home/user/
COPY ./requirements.txt .
RUN python -m pip install -r requirements.txt
WORKDIR /app/

RUN git clone https://github.com/ultralytics/yolov5

COPY ./shared /shared