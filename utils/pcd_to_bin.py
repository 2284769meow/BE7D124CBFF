"""
Script to convert multiple PCD files into other formats using PCL
"""
import os
import subprocess

# Make sure you have PCL installed
package = 'pcl_convert_pcd_ascii_binary'
input_dir = './input'
output_dir = './output'

# Available conversion formats
asciiFormat = '0'
binaryFormat = '1'
binaryCompressedFormat = '2'

for pcd in os.listdir(input_dir):
    # convert all PCD in input directory
    print('Converting pcd :', pcd)
    inputPCD = os.path.join(input_dir, pcd)
    outputPCD = os.path.join(output_dir, ".".join(pcd.split(".")[:-1])) + ".bin"

    # PCL command
    command = [package,
               inputPCD,
               outputPCD,
               binaryCompressedFormat]

    subprocess.call(command)
    break
