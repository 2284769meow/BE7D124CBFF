import numpy as np
import open3d as o3d
import sys
import copy
import matplotlib.pyplot as plt




if __name__ == "__main__":
	cloud = o3d.io.read_point_cloud(sys.argv[1]) # Read the point cloud
	mesh_r = copy.deepcopy(cloud)
	R = mesh_r.get_rotation_matrix_from_xyz((np.pi / 2, 0, np.pi / 4))
	mesh_r.rotate(R, center=(0, 0, 0))

	o3d.visualization.draw_geometries([mesh_r, cloud]) # Visualize the point cloud